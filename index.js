const express = require('express')
const { CloudEvent, HTTPEmitter, HTTPReceiver } = require('cloudevents-sdk')
const PORT = process.env.PORT || 8080
const app = express()
const receiver = new HTTPReceiver()

const welcome = process.env.WELCOME || `🎉 Welcome 😃`
const function_name = process.env.FUNCTION_NAME || `hello`
const function_code = process.env.FUNCTION_CODE 
|| 
`
let ${function_name} = (data) => {
  console.log("😃 received data", data)

  return {
    cloudEventData: {
      type: "dev.knative.demo",
      source: "https://gitlab.com/borg-collective/locutus.wip",
      time: new Date(),
      data: "👋 hello world 🌍"
    },
    response: {
      message: "Hey 🖐️"
    }

  }

}
`

/*
const handle = (data) => {
  return { message: `Hello, ${data.name ? data.name : 'nameless'}` }
}
*/

// Interesting blog post from Axel Rauschmayer
// https://2ality.com/2014/01/eval.html
const code = new Function(`"use strict"; \n${function_code}\nreturn ${function_name}`)

// receiveAndReply responds with new event
const receiveAndReply = (cloudEvent, res) => {
  // received data
  let data = cloudEvent.data
  let headers = HTTPEmitter.headers(cloudEvent)
  let functionResult = code()(data)
  // new cloud event
  let newCloudEventData = functionResult.cloudEventData ? functionResult.cloudEventData : {type: "?", source: "?", time: new Date(), data: "?"}
  let newCloudEvent = new CloudEvent(newCloudEventData)

  let response = functionResult.response ? functionResult.response : { message: "?"}

  console.log(`Event: ${JSON.stringify(newCloudEvent, null, 2)}`)

  res.set(headers)
  res.status(200).send(response)
  // add error management
}

app.use((req, res, next) => {
  let data = ''
  req.setEncoding('utf8')
  req.on('data', chunk => {
    data += chunk
  })
  req.on('end',  _ => {
    req.body = data
    next()
  })
})

app.post('/', (req, res) => {
  try {
    const event = receiver.accept(req.headers, req.body)
    receiveAndReply(event, res)
  } catch (error) {
    console.error("😡", error)
    res.status(415)
      .header('Content-Type', 'application/json')
      .send(JSON.stringify(error))
  }
})

app.listen(PORT, _ => {
  console.log(`🤖 [Locutus.🚧] is listening on ${PORT}!`)
  console.log(welcome)
})
